import numpy as np
from dataclasses import dataclass
from typing import List
from copy import deepcopy as copy
import random
from numpy.random import rand

#################################
#        Data structures        #
#################################
#
# teams = [Name;    TeamID = 0
#          Name;]   TeamID = 1
#
#
#              PF0  PF1
# schedule = [ PF , PF , ...;
#
#         Room0    Room1
# PF = [ PF_room, PF_room, ...;  PF #x
#
#
# jurors = [Juror   JurorID = 0
#           Juror]  JurorID = 1
#
#
# jurorList = [jurorIDs;    PF0
#              jurorIDs;]   PF1
#
#
#                     TeamID=0  TeamID=1  TeamID=2
# judgementMatrix = [   int,      int,      int,      ...;  JurorID = 0
#                       int,      int,      int,      ...;] JurorID = 1
#                       int,      int,      int,      ...;] JurorID = 2


# Juror with a name + affiliations + rooms schedule
@dataclass
class Juror:
    name: str
    affiliations: List[str]
    schedule: List[int]

    def __eq__(self, other):
        if other.name == self.name:
            return True
        else:
            return False

J_list = List[Juror]


# PF in room x with a least of teams and list of jurors
@dataclass
class PF_room:
    team_list: List[str]
    juror_list: J_list
    n_pf: int


R_list = List[PF_room]


# list of PF_room that happen simultaneously
@dataclass
class PF:
    list_pf: R_list
    n_pf: int

    def __getitem__(self, item):
        return self.list_pf[item]

    def __setitem__(self, key, value):
        self.list_pf[key] = value

    def __len__(self):
        return len(self.list_pf)

    def index(self, el):
        return self.list_pf.index(el)


P_list = List[PF]  # schedule type alias (use only for hints in functions)


@dataclass
class Individual:
    schedule: P_list
    jurors: J_list


def import_teams(filename):
    teams = [line.rstrip('\n') for line in open(filename, 'r')]
    return teams


def import_teamSchedule(filename):
    lines = [line.rstrip('\n') for line in open(filename, 'r')]
    nPFs = len(lines)

    pfs = [None] * nPFs
    for index, line in enumerate(lines):
        tmp = line.rstrip(',').split(',')  # remove empty characters at the end
        pfs[index] = list(map(int, tmp))   # convert to int

    nTeams = len(pfs[0])
    nRooms = nTeams // 3

    # mapping consistent with the one used in the matlab code, but with 0-indexing
    schedule = [None] * nPFs
    for iPF in range(nPFs):
        schedule[iPF] = PF([None] * nRooms, iPF)
        for iRoom in range(nRooms):
            schedule[iPF][iRoom] = PF_room(pfs[iPF][iRoom:nTeams:nRooms], [], iPF)
    return schedule


# affiliations are stored in a vector, simply add one after each other with a ';'
def import_jurors(filename, teams):
    jurors = []
    lines = [line.rstrip('\n') for line in open(filename, 'r')]
    for index, line in enumerate(lines):
        fields = line.split(';')
        name = fields[0] + ', ' + fields[1]
        affiliations = []
        for iAff in range(2, len(fields)):
            try:
                affiliation = teams.index(fields[iAff])
            except ValueError:
                affiliation = None
            affiliations += [affiliation]
        juror = Juror(name, affiliations, [])
        jurors.append(juror)
    return jurors


def print_teams(teams):
    print('\nTeams:')
    for teamID, team in enumerate(teams):
        print('\t{:>2}: {}'.format(teamID, team))


def print_jurors(jurors: J_list, teams, schedule):
    print("\nJurors, affiliations:")
    for jurorID, juror in enumerate(jurors):
        str_ = ', '.join([teams[iaff] if iaff is not None else 'None' for iaff in juror.affiliations])
        print('\t{:>2}: {}'.format(jurorID, juror.name) + ' (' + str_ + '), schedule: {},'.format(juror.schedule) +
              '\n\t\t\t\t conflicts: {}, {}, {}'.format(
                  evaluate_conflicts_1j(juror, schedule), evaluate_conflicts_2j(juror, schedule),
                  evaluate_conflicts_3j(juror, schedule)
              ))
    print()


def print_schedule(schedule: P_list, teams):
    print("\nPF Schedule:")
    scores = []
    for iPF, rooms in enumerate(schedule):
        print("  PF {}:".format(iPF))
        p1 = 0
        p2 = 0
        p3 = 0
        for roomID, room in enumerate(rooms):
            str_t = ', '.join([teams[i_team] for i_team in room.team_list])
            j_s = ['{:>2} ({})'.format(j.name, ', '.join([teams[iaff] if iaff is not None else 'None' for iaff in
                                                          j.affiliations])) for j in room.juror_list]
            str_j = '\n\t\t\t\t\t '.join(j_s)
            # print('\tRoom{}: {} teams, {} jurors'.format(roomID, len(room.team_list), len(room.juror_list)))
            # print('\t\t teams:\t' + str_t)
            # print('\t\t\t jurors: '+str_j)
            r1 = evaluate_conflicts_1r(room)
            r2 = evaluate_conflicts_2r(room)
            r3 = evaluate_conflicts_3r(room, schedule)
            # print('\t conflicts: {}, {}, {}\n'.format(r1, r2, r3))
            p1 += r1
            p2 += r2
            p3 += r3
        print('\t PF total:  {}, {}, {}\n'.format(p1, p2, p3))
        scores.append([p1, p2, p3])
    return scores


def randomly_assign_jurors(schedule: P_list, jurors: J_list):
    for pf in schedule:
        random.shuffle(jurors)
        Nr = len(pf)
        for i, juror in enumerate(jurors):
            pf[i % Nr].juror_list.append(jurors[i])
            jurors[i].schedule.append(i % Nr)
    return Individual(schedule, jurors)


# evaluation of rule 1 for a juror in a specific room: no juror can evaluate at team to which he is affiliated
def evaluate_conflicts_1(juror: Juror, pf: PF_room):
    C = 0  # number of conflicts with rule 1 in this pf caused by this juror
    if set(juror.affiliations) & set(pf.team_list):
        C = 1
    return C


# evaluation of rule 2 for a juror in a specific room: no two jurors with same affiliation
def evaluate_conflicts_2(juror: Juror, pf: PF_room):
    C = 0  # number of conflicts with rule 2 in this pf caused by this juror
    for j2 in pf.juror_list:
        if bool(set(juror.affiliations) & set(j2.affiliations)) & (juror != j2):
            C = C+1
    return C


# evaluation of rule 3 for a juror in a specific room: jurors should not see the same team twice
def evaluate_conflicts_3(juror: Juror, pf: PF_room, schedule: P_list):
    C = 0  # number of conflicts with rule 3 in this pf caused by this juror
    for iPF in range(pf.n_pf):
        if set(schedule[iPF][juror.schedule[iPF]].team_list) & set(pf.team_list):
            C = C + 1
    return C


# evaluation of rule 1 in rooms: no juror can evaluate at team to which he is affiliated
def evaluate_conflicts_1r(pf: PF_room):
    C = 0  # number of conflicts with rule 1 in this pf
    for j in pf.juror_list:
        C = C + evaluate_conflicts_1(j, pf)
    return C


# evaluation of rule 2 in rooms: no two jurors with same affiliation
def evaluate_conflicts_2r(pf: PF_room):
    C = 0  # number of conflicts with rule 2 in this pf
    aff_list = [copy(j.affiliations) for j in pf.juror_list]
    for aff_l in aff_list:
        for aff in aff_l:
            counts = 0
            for aff_l2 in aff_list:
                if aff in aff_l2:
                    counts += 1
                    aff_l2.remove(aff)
            C += counts-1
    return C


# evaluation of rule 3 in rooms: jurors should not see the same team twice
def evaluate_conflicts_3r(pf: PF_room, schedule: P_list):
    C = 0  # number of conflicts with rule 3 in this pf
    for j in pf.juror_list:
        C = C + evaluate_conflicts_3(j, pf, schedule)
    return C


# evaluation of rule 1 for single juror: no juror can evaluate at team to which he is affiliated
def evaluate_conflicts_1j(juror: Juror, schedule: P_list):
    C = 0  # number of conflicts with rule 1 caused by this juror
    for i, pf in enumerate(schedule):
        C = C + evaluate_conflicts_1(juror, pf[juror.schedule[i]])
    return C


# evaluation of rule 2 for single juror: no two jurors with same affiliation
def evaluate_conflicts_2j(juror: Juror, schedule: P_list):
    C = 0  # number of conflicts with rule 2 caused by this juror
    for i, pf in enumerate(schedule):
        C = C + evaluate_conflicts_2(juror, pf[juror.schedule[i]])
    return C


# evaluation of rule 3 for single juror: jurors should not see the same team twice
def evaluate_conflicts_3j(juror: Juror, schedule: P_list):
    C = 0  # number of conflicts with rule 3 caused by this juror
    for i, pf in enumerate(schedule):
        C = C + evaluate_conflicts_3(juror, pf[juror.schedule[i]], schedule)
    return C


def resolve_conflicts_1(PFs: PF):
    for pf in PFs:
        rest = [other for other in PFs if other is not pf]  # selecting other rooms

        # computing the score for each juror
        j_score = [0]*len(pf.juror_list)
        for i, juror in enumerate(pf.juror_list):
            j_score[i] = evaluate_conflicts_1(juror, pf)
        j_score_i = np.argsort(j_score)[::-1]

        # for conflicting jurors we swap with a juror in another room
        for i in j_score_i:
            if j_score[i] > 0:
                for other in rest:
                    # computing score in other room
                    next_score = evaluate_conflicts_1(pf.juror_list[i], other)
                    # if score in other room is zero then we look for a suitable swap
                    if next_score == 0:
                        score_here = -1
                        for juror in other.juror_list:
                            # evaluating swap candidate score
                            score_here = evaluate_conflicts_1(juror, pf)
                            # if swap candidate score is 0 then candidate is ok, break loop
                            if score_here == 0:
                                break
                        # we can do the swap now that we have a suitable candidate
                        if score_here == 0:
                            # swapping indices in juror schedules
                            pf.juror_list[i].schedule[PFs.n_pf] = PFs.index(other)
                            juror.schedule[PFs.n_pf] = PFs.index(pf)

                            # swapping jurors in room lists
                            other.juror_list.append(pf.juror_list[i])
                            pf.juror_list.pop(i)

                            pf.juror_list.append(juror)
                            other.juror_list.remove(juror)

                            # swap is done, so no need to check the other rooms
                            break


def compute_j_scores_2(pf: PF_room):
    j_score = [0] * len(pf.juror_list)
    for i, juror in enumerate(pf.juror_list):
        j_score[i] = evaluate_conflicts_2(juror, pf)
    j_score_i = np.argsort(j_score)[::-1]
    return j_score, j_score_i


def resolve_conflicts_2(PFs: PF):
    for pf in PFs:
        rest = [other for other in PFs if other is not pf]  # selecting other rooms
        j_score, j_score_i = compute_j_scores_2(pf)

        for i in j_score_i:
            if j_score[i] > 0:
                for other in rest:
                    next_score = evaluate_conflicts_2(pf.juror_list[i], other) + \
                                 evaluate_conflicts_1(pf.juror_list[i], other)
                    if next_score == 0:
                        score_here = -1
                        for juror in other.juror_list:
                            score_here = evaluate_conflicts_2(juror, pf) + evaluate_conflicts_1(juror, pf)
                            if score_here == 0:
                                break
                        if score_here == 0:
                            # swapping indices in juror schedules
                            pf.juror_list[i].schedule[PFs.n_pf] = PFs.index(other)
                            juror.schedule[PFs.n_pf] = PFs.index(pf)

                            # swapping jurors in room lists
                            other.juror_list.append(pf.juror_list[i])
                            pf.juror_list.pop(i)

                            pf.juror_list.append(juror)
                            other.juror_list.remove(juror)

                            # swap is done, so no need to check the other rooms
                            # though the score of each juror might have changed
                            j_score, _ = compute_j_scores_2(pf)
                            break


def compute_j_scores_3(pf: PF_room, schedule: P_list):
    j_score = [0] * len(pf.juror_list)
    for i, juror in enumerate(pf.juror_list):
        j_score[i] = evaluate_conflicts_3(juror, pf, schedule)
    j_score_i = np.argsort(j_score)[::-1]
    return j_score, j_score_i


def resolve_conflicts_3_by_pairs(PFs: PF, schedule: P_list):
    tot_diff = 0
    for i1, pf1 in enumerate(PFs):
        for pf2 in PFs[i1+1:]:
            js1, js1i = compute_j_scores_3(pf1, schedule)
            js2, js2i = compute_j_scores_3(pf2, schedule)

            # computing matrices of new scores and old scores for all potential swaps
            # and checking for rules 1 and 2
            new_scores = [[99 for _ in pf2.juror_list] for _ in pf1.juror_list]
            old_scores = [[99 for _ in pf2.juror_list] for _ in pf1.juror_list]
            new12 = [[False for _ in pf2.juror_list] for _ in pf1.juror_list]
            for j1 in js1i:
                j1_alt1 = evaluate_conflicts_1(pf1.juror_list[j1], pf2)
                j1_alt2 = evaluate_conflicts_2(pf1.juror_list[j1], pf2)
                j1_alt3 = evaluate_conflicts_3(pf1.juror_list[j1], pf2, schedule)
                for j2 in js2i:
                    j2_alt1 = evaluate_conflicts_1(pf2.juror_list[j2], pf1)
                    j2_alt2 = evaluate_conflicts_2(pf2.juror_list[j2], pf1)
                    j2_alt3 = evaluate_conflicts_3(pf2.juror_list[j2], pf1, schedule)

                    new_scores[j1][j2] = j1_alt3 + j2_alt3
                    old_scores[j1][j2] = js1[j1] + js2[j2]
                    new12[j1][j2] = not bool(j1_alt1 + j1_alt2 + j2_alt1 + j2_alt2)

            # selecting the swaps that respect rules 1 and 2:
            swap_coordinates = []
            score_improvement = []
            for j1, line in enumerate(new12):
                for j2, val in enumerate(line):
                    if val:
                        diff = old_scores[j1][j2] - new_scores[j1][j2]
                        if diff > 0:  # adding only the swaps that would improve the situation
                            swap_coordinates.append([j1, j2])
                            score_improvement.append(diff)

            # within the allowed swaps, find the best improvement and add it to the "to_swap" list
            # then remove the swaps with the indices that have just been used and repeat
            # there is no need to recompute the scores because changing the jurors will not influence that
            final_pairs = []
            Diff = 0
            while len(swap_coordinates) > 0:
                best_i = np.argmax(score_improvement)  # finding the best swap
                Diff += score_improvement[best_i]
                pair = swap_coordinates[best_i]  # taking the corresponding pair

                # adding the two jurors to the swap list
                final_pairs.append([pf1.juror_list[pair[0]], pf2.juror_list[pair[1]]])

                # we remove the potential swaps that involve these two jurors (including the pair we just chose)
                score_improvement = [score_improvement[i] for i, p in enumerate(swap_coordinates)
                                     if ((p[0] != pair[0]) & (p[1] != pair[1]))]
                swap_coordinates = [p for p in swap_coordinates if ((p[0] != pair[0]) & (p[1] != pair[1]))]

            # finally, we can swap them
            for pair in final_pairs:
                # swapping indices in juror schedules
                pair[0].schedule[PFs.n_pf] = PFs.index(pf2)
                pair[1].schedule[PFs.n_pf] = PFs.index(pf1)

                # swapping jurors in room lists
                pf1.juror_list.remove(pair[0])
                pf2.juror_list.remove(pair[1])
                pf2.juror_list.append(pair[0])
                pf1.juror_list.append(pair[1])
            tot_diff += Diff
    return tot_diff


def resolve_conflicts_3_by_triplets(PFs: PF, schedule: P_list):
    tot_diff = 0
    for i1, pf1 in enumerate(PFs):
        for i2, pf2 in enumerate(PFs[i1+1:]):
            for pf3 in PFs[(i1+i2+2):]:
                js1, js1i = compute_j_scores_3(pf1, schedule)
                js2, js2i = compute_j_scores_3(pf2, schedule)
                js3, js3i = compute_j_scores_3(pf3, schedule)

                # computing matrices of new scores and old scores for all type 1 (123) cycles
                # and checking for rules 1 and 2
                new_scores_1 = [[[99 for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                old_scores = [[[99 for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                new12_1 = [[[False for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                for j1 in js1i:
                    j1_alt1 = evaluate_conflicts_1(pf1.juror_list[j1], pf2)
                    j1_alt2 = evaluate_conflicts_2(pf1.juror_list[j1], pf2)
                    j1_alt3 = evaluate_conflicts_3(pf1.juror_list[j1], pf2, schedule)
                    for j2 in js2i:
                        j2_alt1 = evaluate_conflicts_1(pf2.juror_list[j2], pf3)
                        j2_alt2 = evaluate_conflicts_2(pf2.juror_list[j2], pf3)
                        j2_alt3 = evaluate_conflicts_3(pf2.juror_list[j2], pf3, schedule)
                        for j3 in js3i:
                            j3_alt1 = evaluate_conflicts_1(pf3.juror_list[j3], pf1)
                            j3_alt2 = evaluate_conflicts_2(pf3.juror_list[j3], pf1)
                            j3_alt3 = evaluate_conflicts_3(pf3.juror_list[j3], pf1, schedule)

                            new_scores_1[j1][j2][j3] = j1_alt3 + j2_alt3 + j3_alt3
                            old_scores[j1][j2][j3] = js1[j1] + js2[j2] + js3[j3]
                            new12_1[j1][j2][j3] = not bool(j1_alt1 + j1_alt2 + j2_alt1 + j2_alt2 + j3_alt1 + j3_alt2)

                # computing matrices of new scores and old scores for all type 2 (132) cycles
                new_scores_2 = [[[99 for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                new12_2 = [[[False for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                for j1 in js1i:
                    j1_alt1 = evaluate_conflicts_1(pf1.juror_list[j1], pf3)
                    j1_alt2 = evaluate_conflicts_2(pf1.juror_list[j1], pf3)
                    j1_alt3 = evaluate_conflicts_3(pf1.juror_list[j1], pf3, schedule)
                    for j2 in js2i:
                        j2_alt1 = evaluate_conflicts_1(pf2.juror_list[j2], pf1)
                        j2_alt2 = evaluate_conflicts_2(pf2.juror_list[j2], pf1)
                        j2_alt3 = evaluate_conflicts_3(pf2.juror_list[j2], pf1, schedule)
                        for j3 in js3i:
                            j3_alt1 = evaluate_conflicts_1(pf3.juror_list[j3], pf2)
                            j3_alt2 = evaluate_conflicts_2(pf3.juror_list[j3], pf2)
                            j3_alt3 = evaluate_conflicts_3(pf3.juror_list[j3], pf2, schedule)

                            new_scores_2[j1][j2][j3] = j1_alt3 + j2_alt3 + j3_alt3
                            new12_2[j1][j2][j3] = not bool(j1_alt1 + j1_alt2 + j2_alt1 + j2_alt2 + j3_alt1 + j3_alt2)

                # selecting the swaps that respect rules 1 and 2 for type1:
                swap_coordinates_1 = []
                score_improvement_1 = []
                for j1, mat in enumerate(new12_1):
                    for j2, line in enumerate(mat):
                        for j3, val in enumerate(line):
                            if val:
                                diff = old_scores[j1][j2][j3] - new_scores_1[j1][j2][j3]
                                if diff > 0:  # adding only the swaps that would improve the situation
                                    swap_coordinates_1.append([j1, j2, j3])
                                    score_improvement_1.append(diff)

                # selecting the swaps that respect rules 1 and 2 for type2:
                swap_coordinates_2 = []
                score_improvement_2 = []
                for j1, mat in enumerate(new12_2):
                    for j2, line in enumerate(mat):
                        for j3, val in enumerate(line):
                            if val:
                                diff = old_scores[j1][j2][j3] - new_scores_2[j1][j2][j3]
                                if diff > 0:  # adding only the swaps that would improve the situation
                                    swap_coordinates_2.append([j1, j2, j3])
                                    score_improvement_2.append(diff)

                # within the allowed swaps, find the best improvement and add it to the "to_swap" list
                # then remove the swaps with the indices that have just been used and repeat
                # there is no need to recompute the scores because changing the jurors will not influence that
                final_pairs = []
                c_type = []
                Diff = 0
                while len(swap_coordinates_1) + len(swap_coordinates_2) > 0:
                    # finding the best swap
                    if len(swap_coordinates_1) > 0 & len(swap_coordinates_2) > 0:
                        best_i1 = np.argmax(score_improvement_1)
                        best_i2 = np.argmax(score_improvement_2)
                        if score_improvement_1[best_i1] > score_improvement_2[best_i2]:
                            c_type.append(1)
                            best_i = best_i1
                        else:
                            c_type.append(2)
                            best_i = best_i2
                    else:
                        if len(swap_coordinates_1) > 0:
                            best_i = np.argmax(score_improvement_1)
                            c_type.append(1)
                        else:
                            best_i = np.argmax(score_improvement_2)
                            c_type.append(2)

                    if c_type[-1] == 1:
                        Diff += score_improvement_1[best_i]
                        pair = swap_coordinates_1[best_i]
                    else:
                        Diff += score_improvement_2[best_i]
                        pair = swap_coordinates_2[best_i]

                    # adding the two jurors to the swap list
                    final_pairs.append([pf1.juror_list[pair[0]], pf2.juror_list[pair[1]], pf3.juror_list[pair[2]]])

                    # we remove the potential swaps that involve these two jurors (including the pair we just chose)
                    score_improvement_1 = [score_improvement_1[i] for i, p in enumerate(swap_coordinates_1)
                                           if ((p[0] != pair[0]) & (p[1] != pair[1]) & (p[2] != pair[2]))]
                    swap_coordinates_1 = [p for p in swap_coordinates_1 if
                                          ((p[0] != pair[0]) & (p[1] != pair[1]) & (p[2] != pair[2]))]
                    score_improvement_2 = [score_improvement_2[i] for i, p in enumerate(swap_coordinates_2)
                                           if ((p[0] != pair[0]) & (p[1] != pair[1]) & (p[2] != pair[2]))]
                    swap_coordinates_2 = [p for p in swap_coordinates_2 if
                                          ((p[0] != pair[0]) & (p[1] != pair[1]) & (p[2] != pair[2]))]

                # finally, we can swap them
                for i, pair in enumerate(final_pairs):
                    if c_type[i] == 1:
                        # swapping indices in juror schedules
                        pair[0].schedule[PFs.n_pf] = PFs.index(pf2)
                        pair[1].schedule[PFs.n_pf] = PFs.index(pf3)
                        pair[2].schedule[PFs.n_pf] = PFs.index(pf1)

                        # swapping jurors in room lists
                        pf1.juror_list.remove(pair[0])
                        pf2.juror_list.remove(pair[1])
                        pf3.juror_list.remove(pair[2])

                        pf2.juror_list.append(pair[0])
                        pf3.juror_list.append(pair[1])
                        pf1.juror_list.append(pair[2])
                    else:
                        # swapping indices in juror schedules
                        pair[0].schedule[PFs.n_pf] = PFs.index(pf2)
                        pair[1].schedule[PFs.n_pf] = PFs.index(pf1)

                        # swapping jurors in room lists
                        pf1.juror_list.remove(pair[0])
                        pf2.juror_list.remove(pair[1])
                        pf3.juror_list.remove(pair[2])

                        pf2.juror_list.append(pair[2])
                        pf1.juror_list.append(pair[1])
                        pf3.juror_list.append(pair[0])
                tot_diff += Diff
    return tot_diff


# def fix_juror_schedule(ind: Individual):
#
#     for pf in ind.schedule:
#         for j in pf.juror_list:
#             ij = ind.jurors.index(j)
#             ind.jurors[ij] = j


def fix_juror_schedule(ind: Individual, ipf: int):
    for pf in ind.schedule[ipf]:
        for j in pf.juror_list:
            ij = ind.jurors.index(j)
            ind.jurors[ij] = j


def fix_schedule(ind: Individual):
    for pfs in ind.schedule:
        for pf in pfs:
            pf.juror_list = []
    for j in ind.jurors:
        for ipf, r in enumerate(j.schedule):
            ind.schedule[ipf][r].juror_list.append(j)


def computeJ_matrix(schedule: P_list, jurors: J_list):
    nT = 0
    for pf in schedule[0]:
        nT += len(pf.team_list)

    judgementMatrix = np.zeros([len(jurors), nT], dtype=int)
    for i, juror in enumerate(jurors):
        for ipf, room in enumerate(juror.schedule):
            for team in schedule[ipf][room].team_list:
                judgementMatrix[i][team] += 1
    return judgementMatrix


###################################
#        Genetic Algorithm        #
###################################

def initializeIndividual(schedule: P_list, jurors: J_list):
    return randomly_assign_jurors(schedule, jurors)


def initializePopulation(N, schedule: P_list, jurors: J_list):
    population = []
    for _ in range(N):
        population.append(initializeIndividual(copy(schedule), copy(jurors)))
    return population


def evaluateIndividual(individual):
    # #0: jurying his own team,
    # #1: jurors from same country in the same room,
    # #2: jurying the same team twice,
    # #3: difference of team with max number of repeats and min number of repeats
    score = [0]*4
    for PFs in individual.schedule:
        for pf in PFs:
            score[0] += evaluate_conflicts_1r(pf)
            score[1] += evaluate_conflicts_2r(pf)
            score[2] += evaluate_conflicts_3r(pf, schedule)

    j_matrix = computeJ_matrix(individual.schedule, individual.jurors)
    j_matrix[j_matrix > 0] -= 1
    team_sum = j_matrix.sum(axis=0)

    score[3] = max(team_sum) - min(team_sum)

    return 1/(20*score[0] + 5*score[1] + 1*score[2] + 20*score[3]), score


def eval_pop(pop):
    fitness = [None] * len(pop)
    maximumFitness = (None, 0)
    for i, ind in enumerate(pop):
        fit, score = evaluateIndividual(ind)
        fitness[i] = fit
        if fit > maximumFitness[1]:
            maximumFitness = (i, fit)
            best_score = score
    return maximumFitness[0], fitness, best_score


def tournamentSelection(fitness, tournamentParameter):
    populationSize = len(fitness)
    iContestants = [None] * 2
    contestantFitness = [None] * 2
    for i in range(2):
        r = rand()
        iContestants[i] = int(r * populationSize)
        contestantFitness[i] = fitness[iContestants[i]]
    pairs = sorted(zip(contestantFitness, iContestants), key=lambda pair: pair[0], reverse=True)
    contestantFitness, iContestants = [p[0] for p in pairs], [p[1] for p in pairs]
    r = rand()
    if r < tournamentParameter:
        iSelected = iContestants[0]
    else:
        iSelected = iContestants[1]
    return iSelected


# Chooses cross1 with 20% probability and cross2 with 80% probability
def crossover(parent1: Individual, parent2: Individual, probability):
    """This method should cross 'parent1' with 'parent2' and return the resulting children."""
    r = rand()
    if r < 0.2:
        child1, child2 = cross1(parent1, parent2)
    else:
        child1, child2 = cross2(parent1, parent2, probability)
    return child1, child2


# Crosses complete PF juror schedules (e.g. switches the PF2-PF3 schedules of the parents)
def cross1(parent1: Individual, parent2: Individual):
    child1, child2 = copy(parent1), copy(parent2)

    crossoverPoint = int(rand() * len(parent1.schedule))

    for j in range(len(child1.jurors)):
        child1.jurors[j].schedule[:crossoverPoint] = copy(parent2.jurors[j].schedule[:crossoverPoint])
        child2.jurors[j].schedule[crossoverPoint:] = copy(parent1.jurors[j].schedule[crossoverPoint:])

    fix_schedule(child1)
    fix_schedule(child2)
    return child1, child2


# Crosses single PF schedules (e.g. crosses PF2 of the parents)
def cross2(parent1: Individual, parent2: Individual, probability):
    child1, child2 = copy(parent1), copy(parent2)

    for iPFs in range(len(parent1.schedule)):
        r = rand()
        if r < probability:

            for j in range(len(child1.jurors)):
                child1.jurors[j].schedule[iPFs] = -1
                child2.jurors[j].schedule[iPFs] = -1

            pt = parent1.jurors.index(parent2.jurors[0])
            while child1.jurors[pt].schedule[iPFs] == -1:
                child1.jurors[pt].schedule[iPFs] = parent1.jurors[pt].schedule[iPFs]
                pt = parent1.jurors.index(parent2.jurors[pt])

            pt = parent2.jurors.index(parent1.jurors[0])
            while child2.jurors[pt].schedule[iPFs] == -1:
                child2.jurors[pt].schedule[iPFs] = parent2.jurors[pt].schedule[iPFs]
                pt = parent2.jurors.index(parent1.jurors[pt])

            for j in range(len(child1.jurors)):
                if child1.jurors[j].schedule[iPFs] == -1:
                    child1.jurors[j].schedule[iPFs] = parent2.jurors[j].schedule[iPFs]
                if child2.jurors[j].schedule[iPFs] == -1:
                    child2.jurors[j].schedule[iPFs] = parent1.jurors[j].schedule[iPFs]

            fix_schedule(child1)
            fix_schedule(child2)

    return child1, child2


# Mutate individual by swapping the place of two jurors within the same PF schedule
def mutate(individual: Individual, probability):
    """This method should perform a mutation of 'individual' and return the result."""
    mutatedIndividual = copy(individual)
    nJurors = len(individual.jurors)

    for iPF in range(len(individual.schedule)):
        r = rand()
        if r < probability[0]:
            for iJuror1 in range(nJurors):
                r = rand()
                if r < probability[1]:
                    iJuror2 = int(rand() * nJurors)
                    mutatedIndividual.jurors[iJuror1].schedule[iPF], mutatedIndividual.jurors[iJuror2].schedule[iPF] = \
                        mutatedIndividual.jurors[iJuror2].schedule[iPF], mutatedIndividual.jurors[iJuror1].schedule[iPF]

    fix_schedule(mutatedIndividual)

    return mutatedIndividual


#############################################
#     Import team and juror information     #
#############################################
teams = import_teams('teams.txt')
schedule = import_teamSchedule('teams_allocation.txt')
jurors = import_jurors('jurors.txt', teams)
randomly_assign_jurors(schedule, jurors)

for k, pf in enumerate(schedule):
    resolve_conflicts_1(pf)
    resolve_conflicts_2(pf)
    print('rule_3 PF {}'.format(k))
    diff = resolve_conflicts_3_by_triplets(pf, schedule)
    print('\t (3) 1st use: improved score by {}'.format(diff))
    resolve_conflicts_2(pf)
    diff = resolve_conflicts_3_by_triplets(pf, schedule)
    print('\t (3) 2nd use: improved score by {}'.format(diff))
    resolve_conflicts_2(pf)
    diff = resolve_conflicts_3_by_pairs(pf, schedule)
    print('\t (2) 1st use: improved score by {}'.format(diff))
    diff = resolve_conflicts_3_by_pairs(pf, schedule)
    print('\t (2) 2nd use: improved score by {}'.format(diff))

# print what we have

# print_teams(teams)
# print_jurors(jurors, teams, schedule)
print_schedule(schedule, teams)

nJ = len(jurors)
nPFs = len(schedule)

#################################
#     Run Genetic Algorithm     #
#################################
# populationSize = 200
# nGenerations = 100000
# tournamentParameter = 0.75
# crossoverProbability = 0.8
# mutationProbability = (1/nPFs, 1/nJ)
#
# population = initializePopulation(populationSize, schedule, jurors)
# for iGeneration in range(nGenerations):
#     try:             # --> python trick: press ctrl + C to stop the breeding
#         # Evaluation
#         best_i, fitness, best_score = eval_pop(population)
#         if iGeneration % 20 == 0:
#             print('{:6}{:6}   {}'.format(iGeneration, int(1 / fitness[best_i]), best_score))
#
#         tmpPopulation = [None] * populationSize
#         # Selection
#         for i in range(populationSize // 2):
#             i1 = tournamentSelection(fitness, tournamentParameter)
#             i2 = tournamentSelection(fitness, tournamentParameter)
#
#             # Crossover
#             child1, child2 = crossover(population[i1], population[i2], crossoverProbability)
#             tmpPopulation[2 * i] = child1
#             tmpPopulation[2 * i + 1] = child2
#
#         # Insert new, random Individual(s) at the end of the temporary population list
#         for i in range(2 + populationSize % 2):
#             tmpPopulation[-1 - i] = initializeIndividual(copy(schedule), copy(jurors))
#
#         # Mutate all individuals in the temporary population list
#         for ind in tmpPopulation:
#             ind = mutate(ind, mutationProbability)
#             for k, pf in enumerate(ind.schedule):
#                 resolve_conflicts_1(pf)
#                 resolve_conflicts_2(pf)
#
#         # Insert Best Individual at the beginning of the temporary population list
#         tmpPopulation[0] = copy(population[best_i])
#
#         # Update population
#         population = tmpPopulation
#
#     except KeyboardInterrupt:  # stop breeding if prompted
#         break


###########################
#     Save the result     #
###########################
# best_ind = population[0]
# schedule = best_ind.schedule
# jurors = best_ind.jurors

j_matrix = computeJ_matrix(schedule, jurors)

j_matrix[j_matrix > 0] -= 1
team_sum = j_matrix.sum(axis=0)

print(max(team_sum) - min(team_sum))
print(sum([line.sum() for line in j_matrix]))

##########################
# Print judgement matrix #
##########################
# print_matrix(j_matrix)
# print("Final fitness: {} with issues {}".format(1/fitness[best_i], best_score))
