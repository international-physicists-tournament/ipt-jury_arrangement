import numpy as np
from dataclasses import dataclass
from typing import List
from copy import deepcopy as copy
import random
import sys
from numpy.random import rand
from itertools import chain

#################################
#        Data structures        #
#################################
#
# teams = [Name;    TeamID = 0
#          Name;]   TeamID = 1
#
#
#              PF0  PF1
# schedule = [ PF , PF , ...;
#
#         Room0    Room1
# PF = [ PF_room, PF_room, ...;  PF #x
#
#
# jurors = [Juror   JurorID = 0
#           Juror]  JurorID = 1
#
#
# jurorList = [jurorIDs;    PF0
#              jurorIDs;]   PF1
#
#
#                     TeamID=0  TeamID=1  TeamID=2
# judgementMatrix = [   int,      int,      int,      ...;  JurorID = 0
#                       int,      int,      int,      ...;] JurorID = 1
#                       int,      int,      int,      ...;] JurorID = 2


# Juror with a name + affiliations + rooms schedule
@dataclass
class Juror:
    name: str
    affiliations: List[str]
    schedule: List[int]
    teams_seen: List[int]

    def __eq__(self, other):
        if other.name == self.name:
            return True
        else:
            return False


J_list = List[Juror]


# PF in room x with a least of teams and list of jurors
@dataclass
class PF_room:
    team_list: List[str]
    juror_list: J_list
    n_pf: int


R_list = List[PF_room]


# list of PF_room that happen simultaneously
@dataclass
class PF:
    list_pf: R_list
    n_pf: int
    Nj: int
    tot_n_j: int
    max_j_p_room: int

    def __getitem__(self, item):
        return self.list_pf[item]

    def __setitem__(self, key, value):
        self.list_pf[key] = value

    def __len__(self):
        return len(self.list_pf)

    def index(self, el):
        return self.list_pf.index(el)


P_list = List[PF]  # schedule type alias (use only for hints in functions)


@dataclass
class Individual:
    schedule: P_list
    jurors: J_list


# useful global variables that we set in the import
Nj = 0
Nr = 0
Nt = 0

nPFs = 0

# main global variables
teams = []
schedule = []
jurors = []
pfJurors = []


# imports everything in the right order and fills the variables teams, schedule and jurors
def import_all(file_teams, file_schedule, file_jurors):
    import_teams(file_teams)
    import_teamSchedule(file_schedule)
    import_jurors(file_jurors)

    global schedule
    # now that we imported everything we can decide the max number of jurors per room
    for i, pf in enumerate(schedule):
        pf.tot_n_j = Nj
        for j in jurors:
            if j.schedule[i] == -2:
                pf.tot_n_j -= 1
        pf.max_j_p_room = (pf.tot_n_j-1)//Nr + 1


def import_teams(filename):
    global Nr
    global Nt
    global teams

    teams = [line.rstrip('\n') for line in open(filename, 'r')]

    # set the global variables
    Nt = len(teams)
    Nr = Nt//3


def import_teamSchedule(filename):
    global schedule
    global nPFs
    lines = [line.rstrip('\n') for line in open(filename, 'r')]
    nPFs = len(lines)

    pfs = [None] * nPFs
    for index, line in enumerate(lines):
        tmp = line.rstrip(',').split(',')  # remove empty characters at the end
        pfs[index] = list(map(int, tmp))   # convert to int

    nTeams = len(pfs[0])
    nRooms = nTeams // 3

    # mapping consistent with the one used in the matlab code, but with 0-indexing
    schedule = [None] * nPFs
    for iPF in range(nPFs):
        schedule[iPF] = PF([None] * nRooms, iPF, 0,0,0)
        for iRoom in range(nRooms):
            schedule[iPF][iRoom] = PF_room(pfs[iPF][iRoom:nTeams:nRooms], [], iPF)


# affiliations are stored in a vector, simply add one after each other with a ';'
# FIRST IMPORT TEAMS, THEN THE JURORS
def import_jurors(filename):
    global Nj
    global jurors
    global pfJurors

    jurors = []
    lines = [line.rstrip('\n') for line in open(filename, 'r')]
    for index, line in enumerate(lines):
        fields = line.split(';')
        name = fields[0] + ', ' + fields[1]
        affiliations = []
        for iAff in range(3, len(fields)):
            try:
                affiliation = teams.index(fields[iAff])
                affiliations += [affiliation]
            except ValueError:
                pass

        j_sched = [-1]*nPFs
        if len(fields[2])>0:
            j_not_there = fields[2].split(',')
            for r in j_not_there:
                j_sched[int(r)-1] = -2
        juror = Juror(name, affiliations, j_sched, [0]*Nt)
        jurors.append(juror)
    Nj = len(jurors)


def print_teams(teams):
    print('\nTeams:')
    for teamID, team in enumerate(teams):
        print('\t{:>2}: {}'.format(teamID, team))


def print_jurors(jurors: J_list, teams, schedule):
    print("\nJurors, affiliations:")
    for jurorID, juror in enumerate(jurors):
        str_ = ', '.join([teams[iaff] if iaff is not None else 'None' for iaff in juror.affiliations])
        print('\t{:>2}: {}'.format(jurorID, juror.name) + ' (' + str_ + '), schedule: {},'.format(juror.schedule) +
              '\n\t\t\t\t conflicts: {}, {}, {}'.format(
                  evaluate_conflicts_1j(juror, schedule), evaluate_conflicts_2j(juror, schedule),
                  evaluate_conflicts_3j(juror, schedule)
              ))
    print()


def print_schedule(schedule: P_list, teams):
    global jurors
    print("\nPF Schedule:")
    for iPF, rooms in enumerate(schedule):
        print("  PF {}:".format(iPF))
        p1 = 0
        p2 = 0
        p3 = 0
        for roomID, room in enumerate(rooms):
            str_t = ', '.join([teams[i_team] for i_team in room.team_list])
            j_s = ['{:>2} ({})'.format(j.name, ', '.join([teams[iaff] if iaff is not None else 'None' for iaff in
                                                          j.affiliations])) for j in room.juror_list]
            str_j = '\n\t\t\t\t '.join(j_s)
            print('\tRoom{}: {} teams, {} jurors'.format(roomID, len(room.team_list), len(room.juror_list)))
            print('\t\t teams:\t' + str_t)
            print('\t\t\t jurors: '+str_j)
            r1 = evaluate_conflicts_1r(room)
            r2 = evaluate_conflicts_2r(room)
            r3 = evaluate_conflicts_3r(room)
            print('\t conflicts: {}, {}, {}\n'.format(r1, r2, r3))
            p1 += r1
            p2 += r2
            p3 += r3
        print('\t PF total:  {}, {}, {}\n'.format(p1, p2, p3))

    scores = evaluate_conflicts(schedule, jurors)
    print('total scores for whole schedule: {}\n'.format(scores))
    return scores


def randomly_assign_jurors(schedule: P_list, jurors: J_list):
    for pf in schedule:
        random.shuffle(jurors)
        Nr = len(pf)
        for i, juror in enumerate(jurors):
            pf[i % Nr].juror_list.append(jurors[i])
            jurors[i].schedule.append(i % Nr)
    return Individual(schedule, jurors)


# evaluation of rule 1 for a juror in a specific room: no juror can evaluate at team to which he is affiliated
def evaluate_conflicts_1(juror: Juror, pf: PF_room):
    C = 0  # number of conflicts with rule 1 in this pf caused by this juror
    if set(juror.affiliations) & set(pf.team_list):
        C = 1
    return C


# evaluation of rule 2 for a juror in a specific room: no two jurors with same affiliation
def evaluate_conflicts_2(juror: Juror, pf: PF_room):
    C = 0  # number of conflicts with rule 2 in this pf caused by this juror
    for j2 in pf.juror_list:
        if bool(set(juror.affiliations) & set(j2.affiliations)) & (juror != j2):
            C = C+1
            print('{} ({}) '.format(juror.name,juror.affiliations))
            print('{} ({}) '.format(j2.name,j2.affiliations))
            print([j.name for j in pf.juror_list])
            print(' ')
    return C


# evaluation of rule 3 for a juror in a specific room: jurors should not see the same team twice
#assuming the juror is not yet added in the room!!!!
def evaluate_conflicts_3(juror: Juror, pf: PF_room):
    C = 0  # number of conflicts with rule 3 in this pf caused by this juror
    for t in pf.team_list:
        C += juror.teams_seen[t]
    return C


# evaluation of rule 1 in rooms: no juror can evaluate at team to which he is affiliated
def evaluate_conflicts_1r(pf: PF_room):
    C = 0  # number of conflicts with rule 1 in this pf
    for j in pf.juror_list:
        C = C + evaluate_conflicts_1(j, pf)
    return C


# evaluation of rule 2 in rooms: no two jurors with same affiliation
def evaluate_conflicts_2r(pf: PF_room):
    C = 0  # number of conflicts with rule 2 in this pf
    jurors = copy(pf.juror_list)
    affs = []
    for j in pf.juror_list:
        for aff in j.affiliations:
            if aff in affs:
                C += 1
            else:
                affs.append(aff)
    return C


# evaluation of rule 3 in rooms: jurors should not see the same team twice
def evaluate_conflicts_3r(pf: PF_room):
    C = 0  # number of conflicts with rule 3 in this pf
    for j in pf.juror_list:
        C += evaluate_conflicts_3(j, pf)
    return C


# evaluation of rule 1 for single juror: no juror can evaluate at team to which he is affiliated
def evaluate_conflicts_1j(juror: Juror, schedule: P_list):
    C = 0  # number of conflicts with rule 1 caused by this juror
    for i, pf in enumerate(schedule):
        if juror.schedule[i] != -2:
            C = C + evaluate_conflicts_1(juror, pf[juror.schedule[i]])
    return C


# evaluation of rule 2 for single juror: no two jurors with same affiliation
def evaluate_conflicts_2j(juror: Juror, schedule: P_list):
    C = 0  # number of conflicts with rule 2 caused by this juror
    for i, pf in enumerate(schedule):
        if juror.schedule[i] != -2:
            C = C + evaluate_conflicts_2(juror, pf[juror.schedule[i]])
    return C


# evaluation of rule 3 for single juror: jurors should not see the same team twice
def evaluate_conflicts_3j(juror: Juror, schedule: P_list):
    C = 0  # number of conflicts with rule 3 caused by this juror
    for i, pf in enumerate(schedule):
        if juror.schedule[i] != -2:
            C = C + evaluate_conflicts_3(juror, pf[juror.schedule[i]])
    return C


def compute_j_scores_2(pf: PF_room):
    j_score = [0] * len(pf.juror_list)
    for i, juror in enumerate(pf.juror_list):
        j_score[i] = evaluate_conflicts_2(juror, pf)
    j_score_i = np.argsort(j_score)[::-1]
    return j_score, j_score_i


def compute_j_scores_3(pf: PF_room):
    # we substract the number of teams in the room because the juror is already in the room
    j_score = [evaluate_conflicts_3(juror, pf)-len(pf.team_list) for juror in pf.juror_list]
    j_score_i = np.argsort(j_score)[::-1]
    return j_score, j_score_i


def make_score3_better():
    global schedule
    global jurors
    for k in chain(range(nPFs-1, 1, -1), range(1, nPFs)):
        score = evaluate_conflicts(schedule, jurors)
        b_score = score[2]

        print('\t round {}, current score: {}'.format(k+1, b_score))
        pop = [copy(schedule)]
        scores = [b_score]
        parents = pop
        while True:
            c_score = b_score
            n_pop = []
            for el in parents:
                children, children_scores = test_solve_conflicts_3(k, el)
                n_pop += children
                scores += children_scores
            pop += n_pop

            b_i = np.argmin(scores)
            b_score = scores[b_i]
            b_el = pop[b_i]
            parents = n_pop

            print('\t\t population: {}, best score overall: {}'.format(len(pop), b_score))

            if b_score == c_score:
                print('\t\t\t ----> this population will not get better')
                break
        schedule = b_el
        jurors = []
        for pf in schedule[k]:
            for j in pf.juror_list:
                jurors += [j]


def check_violation_1(j: Juror, r: PF_room):
    for aff in j.affiliations:
        if aff in r.team_list:
            return True
    return False

def check_violation_2(j: Juror, r: PF_room):
    for j2 in r.juror_list:
        for aff in j.affiliations:
            if aff in j2.affiliations:
                return True
    return False

def find_best_swap(PFs: PF):
    best_diff = 0
    j_swap = [PFs[0].juror_list[0],PFs[0].juror_list[0]]
    for i1, pf1 in enumerate(PFs):
        for pf2 in PFs[i1+1:]:
            swap_coordinates = []
            score_improvement = []

            #computing number of current rule 3 violations for each juror
            js1, _ = compute_j_scores_3(pf1)
            js2, _ = compute_j_scores_3(pf2)

            for ij1, j1 in enumerate(pf1.juror_list):

                # we start by checking if this juror can go in this room with rule 1
                if check_violation_1(j1, pf2):
                    continue
                # if there is one juror with the same affiliation in the room then
                # we can check if swapping with that one improves the situation
                # otherwise we loop through each of them
                one_swap = -1
                for ij2, j2 in enumerate(pf2.juror_list):
                    for aff in j1.affiliations:
                        if aff in j2.affiliations:
                            one_swap = ij2
                if one_swap == -1:
                    jpf2 = pf2.juror_list
                else:
                    jpf2 = [pf2.juror_list[one_swap]]

                # we compute the new score of j1 in the new room
                ns1 = evaluate_conflicts_3(j1,pf2)
                for ij2, j2 in enumerate(jpf2):
                    # we check wether the other juror can come in this room
                    if check_violation_1(j2,pf1):
                        continue
                    if one_swap == -1 and check_violation_2(j2, pf1):
                        continue
                    # we now compute the improvement of the swap
                    ns2 = evaluate_conflicts_3(j2,pf1)
                    if one_swap == -1:
                        diff = js1[ij1] + js2[ij2] - ns1 - ns2
                    else:
                        diff = js1[ij1] + js2[one_swap] - ns1 - ns2

                    if diff > best_diff:
                        j_swap = [j1, j2]
                        best_diff = diff

    # we return the swap of jurors which would best improve the score
    return best_diff, j_swap

def make_score3_betterv2():
    global schedule
    global jurors

    while True:
        best_diff = 0
        for i, pf in enumerate(schedule):
            diff, j = find_best_swap(pf)
            if diff > best_diff:
                best_diff = diff
                I = i
                j_swap = j
        if best_diff > 0:
            print('\t found swap for PF{}: {} (room {}) <---> {} (room {})'.format(I,j_swap[0].name,j_swap[0].schedule[I],j_swap[1].name,j_swap[1].schedule[I]))
            print('\t\t ------> score improved by {}\n'.format(best_diff))

            # removing the jurors from the rooms
            schedule[I][j_swap[0].schedule[I]].juror_list.remove(j_swap[0])
            schedule[I][j_swap[1].schedule[I]].juror_list.remove(j_swap[1])

            # changing the list of teams seen by the two jurors
            for t in schedule[I][j_swap[0].schedule[I]].team_list:
                j_swap[0].teams_seen[t] -= 1
                j_swap[1].teams_seen[t] += 1
            for t in schedule[I][j_swap[1].schedule[I]].team_list:
                j_swap[1].teams_seen[t] -= 1
                j_swap[0].teams_seen[t] += 1

            # changing the jurors' schedules
            tmp = j_swap[0].schedule[I]
            j_swap[0].schedule[I] = j_swap[1].schedule[I]
            j_swap[1].schedule[I] = tmp

            # adding the jurors in the new rooms
            schedule[I][j_swap[0].schedule[I]].juror_list.append(j_swap[0])
            schedule[I][j_swap[1].schedule[I]].juror_list.append(j_swap[1])

        else:
            score = evaluate_conflicts(schedule, jurors)
            print('\t ####### No improving swap found ------> current score: {} #######'.format(score[2]))
            break


def test_solve_conflicts_3(iPF: int, schedule: P_list):
    cps = [copy(schedule) for _ in range(2)]

    resolve_conflicts_3_by_pairs(iPF, cps[0])
    resolve_conflicts_3_by_triplets(iPF, cps[1])

    scores = []
    for cp in cps:
        jurs = []
        for pf in cp[iPF]:
            for j in pf.juror_list:
                jurs += [j]
        score = evaluate_conflicts(cp, jurs)
        if (score[0] + score[1]) > 0:
            cps.remove(cp)
        else:
            scores += [score[2]]

    return cps, scores


def resolve_conflicts_3_by_pairs(iPF: int, schedule: P_list):
    PFs = schedule[iPF]
    tot_diff = 0
    for i1, pf1 in enumerate(PFs):
        for pf2 in PFs[i1+1:]:
            js1, js1i = compute_j_scores_3(pf1)
            js2, js2i = compute_j_scores_3(pf2)

            # computing matrices of new scores and old scores for all potential swaps
            # and checking for rules 1 and 2
            new_scores = [[99 for _ in pf2.juror_list] for _ in pf1.juror_list]
            old_scores = [[99 for _ in pf2.juror_list] for _ in pf1.juror_list]
            new12 = [[False for _ in pf2.juror_list] for _ in pf1.juror_list]
            for j1 in js1i:
                j1_alt1 = evaluate_conflicts_1(pf1.juror_list[j1], pf2)
                j1_alt2 = evaluate_conflicts_2(pf1.juror_list[j1], pf2)
                j1_alt3 = evaluate_conflicts_3(pf1.juror_list[j1], pf2)
                for j2 in js2i:
                    j2_alt1 = evaluate_conflicts_1(pf2.juror_list[j2], pf1)
                    j2_alt2 = evaluate_conflicts_2(pf2.juror_list[j2], pf1)
                    j2_alt3 = evaluate_conflicts_3(pf2.juror_list[j2], pf1)

                    new_scores[j1][j2] = j1_alt3 + j2_alt3
                    old_scores[j1][j2] = js1[j1] + js2[j2]
                    new12[j1][j2] = not bool(j1_alt1 + j1_alt2 + j2_alt1 + j2_alt2)

            # selecting the swaps that respect rules 1 and 2:
            swap_coordinates = []
            score_improvement = []
            for j1, line in enumerate(new12):
                for j2, val in enumerate(line):
                    if val:
                        diff = old_scores[j1][j2] - new_scores[j1][j2]
                        if diff > 0:  # adding only the swaps that would improve the situation
                            swap_coordinates.append([j1, j2])
                            score_improvement.append(diff)

            # within the allowed swaps, find the best improvement and do it
            if len(swap_coordinates) > 0:
                best_i = np.argmax(score_improvement)  # finding the best swap
                Diff = score_improvement[best_i]
                pair_i = swap_coordinates[best_i]  # taking the corresponding pair

                pair = [pf1.juror_list[pair_i[0]], pf2.juror_list[pair_i[1]]]
                # swapping indices in juror schedules
                pair[0].schedule[PFs.n_pf] = PFs.index(pf2)
                pair[1].schedule[PFs.n_pf] = PFs.index(pf1)

                # swapping jurors in room lists
                pf1.juror_list.remove(pair[0])
                pf2.juror_list.remove(pair[1])
                pf2.juror_list.append(pair[0])
                pf1.juror_list.append(pair[1])

                tot_diff += Diff
    return tot_diff


def resolve_conflicts_3_by_triplets(iPF: int, schedule: P_list):
    tot_diff = 0
    PFs = schedule[iPF]
    for i1, pf1 in enumerate(PFs):
        for i2, pf2 in enumerate(PFs[i1+1:]):
            for pf3 in PFs[(i1+i2+2):]:
                js1, js1i = compute_j_scores_3(pf1)
                js2, js2i = compute_j_scores_3(pf2)
                js3, js3i = compute_j_scores_3(pf3)

                # computing matrices of new scores and old scores for all type 1 (123) cycles
                # and checking for rules 1 and 2
                new_scores_1 = [[[99 for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                old_scores = [[[99 for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                new12_1 = [[[False for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                for j1 in js1i:
                    j1_alt1 = evaluate_conflicts_1(pf1.juror_list[j1], pf2)
                    j1_alt2 = evaluate_conflicts_2(pf1.juror_list[j1], pf2)
                    j1_alt3 = evaluate_conflicts_3(pf1.juror_list[j1], pf2)
                    for j2 in js2i:
                        j2_alt1 = evaluate_conflicts_1(pf2.juror_list[j2], pf3)
                        j2_alt2 = evaluate_conflicts_2(pf2.juror_list[j2], pf3)
                        j2_alt3 = evaluate_conflicts_3(pf2.juror_list[j2], pf3)
                        for j3 in js3i:
                            j3_alt1 = evaluate_conflicts_1(pf3.juror_list[j3], pf1)
                            j3_alt2 = evaluate_conflicts_2(pf3.juror_list[j3], pf1)
                            j3_alt3 = evaluate_conflicts_3(pf3.juror_list[j3], pf1)

                            new_scores_1[j1][j2][j3] = j1_alt3 + j2_alt3 + j3_alt3
                            old_scores[j1][j2][j3] = js1[j1] + js2[j2] + js3[j3]
                            new12_1[j1][j2][j3] = not bool(j1_alt1 + j1_alt2 + j2_alt1 + j2_alt2 + j3_alt1 + j3_alt2)

                # computing matrices of new scores and old scores for all type 2 (132) cycles
                new_scores_2 = [[[99 for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                new12_2 = [[[False for _ in pf3.juror_list] for _ in pf2.juror_list] for _ in pf1.juror_list]
                for j1 in js1i:
                    j1_alt1 = evaluate_conflicts_1(pf1.juror_list[j1], pf3)
                    j1_alt2 = evaluate_conflicts_2(pf1.juror_list[j1], pf3)
                    j1_alt3 = evaluate_conflicts_3(pf1.juror_list[j1], pf3)
                    for j2 in js2i:
                        j2_alt1 = evaluate_conflicts_1(pf2.juror_list[j2], pf1)
                        j2_alt2 = evaluate_conflicts_2(pf2.juror_list[j2], pf1)
                        j2_alt3 = evaluate_conflicts_3(pf2.juror_list[j2], pf1)
                        for j3 in js3i:
                            j3_alt1 = evaluate_conflicts_1(pf3.juror_list[j3], pf2)
                            j3_alt2 = evaluate_conflicts_2(pf3.juror_list[j3], pf2)
                            j3_alt3 = evaluate_conflicts_3(pf3.juror_list[j3], pf2)

                            new_scores_2[j1][j2][j3] = j1_alt3 + j2_alt3 + j3_alt3
                            new12_2[j1][j2][j3] = not bool(j1_alt1 + j1_alt2 + j2_alt1 + j2_alt2 + j3_alt1 + j3_alt2)

                # selecting the swaps that respect rules 1 and 2 for type1:
                swap_coordinates_1 = []
                score_improvement_1 = []
                for j1, mat in enumerate(new12_1):
                    for j2, line in enumerate(mat):
                        for j3, val in enumerate(line):
                            if val:
                                diff = old_scores[j1][j2][j3] - new_scores_1[j1][j2][j3]
                                if diff > 0:  # adding only the swaps that would improve the situation
                                    swap_coordinates_1.append([j1, j2, j3])
                                    score_improvement_1.append(diff)

                # selecting the swaps that respect rules 1 and 2 for type2:
                swap_coordinates_2 = []
                score_improvement_2 = []
                for j1, mat in enumerate(new12_2):
                    for j2, line in enumerate(mat):
                        for j3, val in enumerate(line):
                            if val:
                                diff = old_scores[j1][j2][j3] - new_scores_2[j1][j2][j3]
                                if diff > 0:  # adding only the swaps that would improve the situation
                                    swap_coordinates_2.append([j1, j2, j3])
                                    score_improvement_2.append(diff)

                # within the allowed swaps, find the best improvement and perform it
                if len(swap_coordinates_1) + len(swap_coordinates_2) > 0:
                    if len(swap_coordinates_1) > 0 & len(swap_coordinates_2) > 0:
                        best_i1 = np.argmax(score_improvement_1)
                        best_i2 = np.argmax(score_improvement_2)
                        if score_improvement_1[best_i1] > score_improvement_2[best_i2]:
                            c_type = 1
                            best_i = best_i1
                        else:
                            c_type = 2
                            best_i = best_i2
                    else:
                        if len(swap_coordinates_1) > 0:
                            best_i = np.argmax(score_improvement_1)
                            c_type = 1
                        else:
                            best_i = np.argmax(score_improvement_2)
                            c_type = 2
                    if c_type == 1:
                        Diff = score_improvement_1[best_i]
                        pair_i = swap_coordinates_1[best_i]

                        pair = [pf1.juror_list[pair_i[0]], pf2.juror_list[pair_i[1]], pf3.juror_list[pair_i[2]]]

                        # swapping indices in juror schedules
                        pair[0].schedule[PFs.n_pf] = PFs.index(pf2)
                        pair[1].schedule[PFs.n_pf] = PFs.index(pf3)
                        pair[2].schedule[PFs.n_pf] = PFs.index(pf1)

                        # swapping jurors in room lists
                        pf1.juror_list.remove(pair[0])
                        pf2.juror_list.remove(pair[1])
                        pf3.juror_list.remove(pair[2])

                        pf2.juror_list.append(pair[0])
                        pf3.juror_list.append(pair[1])
                        pf1.juror_list.append(pair[2])
                    else:
                        Diff = score_improvement_2[best_i]
                        pair_i = swap_coordinates_2[best_i]

                        pair = [pf1.juror_list[pair_i[0]], pf2.juror_list[pair_i[1]], pf3.juror_list[pair_i[2]]]

                        # swapping indices in juror schedules
                        pair[0].schedule[PFs.n_pf] = PFs.index(pf3)
                        pair[1].schedule[PFs.n_pf] = PFs.index(pf1)
                        pair[2].schedule[PFs.n_pf] = PFs.index(pf2)

                        # swapping jurors in room lists
                        pf1.juror_list.remove(pair[0])
                        pf2.juror_list.remove(pair[1])
                        pf3.juror_list.remove(pair[2])

                        pf2.juror_list.append(pair[2])
                        pf1.juror_list.append(pair[1])
                        pf3.juror_list.append(pair[0])

                    tot_diff += Diff
    return tot_diff


def computeJ_matrix(schedule: P_list, jurors: J_list):
    judgementMatrix = np.array([j.teams_seen for j in jurors])

    # judgementMatrix = np.zeros([len(jurors), Nt], dtype=int)
    # for i, juror in enumerate(jurors):
    #     for ipf, room in enumerate(juror.schedule):
    #         for team in schedule[ipf][room].team_list:
    #             judgementMatrix[i][team] += 1
    return judgementMatrix


def print_matrix(matrix):
    sep1 = '  '
    sep2 = ' | '
    mat = matrix.copy()
    ncols = mat.shape[1]

    print()
    print((2+len(sep2))*' ' + ' '.join(['{:>2}'.format(item) for item in range(ncols)]))
    print((2+len(sep2))*' ' + (3*(mat.shape[1]-1)+1 + 2) * '-')
    for i, line in enumerate(mat):
        rowsum = sum([max(0, item-1) for item in line])
        print('{:>2}: | '.format(i)+sep1.join([str(item) for item in line]) + ' | ' + str(rowsum))
    print((2+len(sep2))*' ' + (3*(ncols-1)+1 + 2) * '-')
    mat[mat > 0] -= 1
    colsum = mat.sum(axis=0)
    print((3+len(sep2))*' ' + sep1.join([str(item) for item in colsum]))


def evaluate_conflicts(schedule: P_list, jurors: J_list):
    # #0: jurying his own team,
    # #1: jurors from same country in the same room,
    # #2: jurying the same team twice,
    # #3: difference of team with max number of repeats and min number of repeats
    # #4: jurying the same team more than twice,
    score = [0]*5
    for PFs in schedule:
        for pf in PFs:
            score[0] += evaluate_conflicts_1r(pf)
            score[1] += evaluate_conflicts_2r(pf)

    j_matrix = computeJ_matrix(schedule, jurors)
    # print("judgement matrix:")
    # print_matrix(j_matrix)

    j_matrix[j_matrix > 0] -= 1
    team_sum = j_matrix.sum(axis=0)

    score[3] = max(team_sum) - min(team_sum)

    score[2] = sum([line.sum() for line in j_matrix])

    j_matrix[j_matrix > 0] -= 1
    score[4] = sum([line.sum() for line in j_matrix])

    # fit = 1 / (20 * score[0] + 5 * score[1] + 1 * score[2] + 20 * score[3])
    return score


def conflict_add_rule12(i: int, j: int, k: int):
    # check rule 1: juror cannot be affiliated to one of the teams
    for aff in jurors[k].affiliations:
        if aff in schedule[i][j].team_list:
            return True

    # check rule 2: jurors in the same room cannot be affiliated to one another
    for j2 in schedule[i][j].juror_list:
        for aff in j2.affiliations:
            for aff2 in jurors[k].affiliations:
                if aff == aff2:
                    return True

    return False


def conflict_add_rule3(i: int, j: int, k: int):
    # respect this rule with a certain probability,
    # so that it is a little more relaxed and quick
    # but we cannot have a juror that sees three time the same team

    for t in schedule[i][j].team_list:
        if jurors[k].teams_seen[t] > 1:
            return True

    if rand() < prob_ign_rule3:
        return False

    for t in schedule[i][j].team_list:
        if jurors[k].teams_seen[t] > 0:
            return True
    return False


def evaluate_new_juror(i: int, j: int, k: int):
    # check juror is not already in another room for this round
    if jurors[k].schedule[i] != -1:
        return False

    # if adding this juror in this room violates rule 1 or 2 then we do not add him
    if conflict_add_rule12(i, j, k):
        return False

    # if adding this juror in this room violates rule 3 then we do not add him
    if (i > 0) & conflict_add_rule3(i, j, k):
        return False

    return True


def show_status(i: int):
    perc = schedule[i].Nj/schedule[i].tot_n_j

    print('[' + '*'*round(perc*50) + '-'*round((1-perc)*50) + ']  ' + str(round(perc*100)) + '%')


def show_status_global():
    n = 0
    for i in range(nPFs):
        n += schedule[i].Nj

    perc = n/(nPFs*schedule[i].tot_n_j)
    sys.stdout.write('[' + '*'*round(perc*50) + '-'*round((1-perc)*50) + ']  ' + str(round(perc*100)) + '%, ' +
                     str(count))


# function to check if list of jurors is coeherent with the schedule
def is_coherent(schedule: P_list, jurors: J_list):
    for i, Pfs in enumerate(schedule):
        for j, pf in enumerate(Pfs):
            for juror in pf.juror_list:
                k = jurors.index(juror)
                if jurors[k].schedule[i] != j:
                    return False
    return True


# this is to have the same linked objects at the end
def extract_real_jurors(schedule: P_list):
    jurors = []
    for PFs in schedule:
        for pf in PFs:
            for j in pf.juror_list:
                if j not in jurors:
                    jurors.append(j)
    jurors.sort(key=lambda x: x.name)
    return jurors

###################################
#    Smart Ordering Algorithms    #
###################################


def jurors_ordering():
    global jurors
    random.shuffle(jurors)

    aff_team_list = [[] for _ in range(Nt)]
    for k, juror in enumerate(jurors):
        if len(juror.affiliations) == 1:
            aff_team_list[juror.affiliations[0]] += [k]

    aff_count = []
    nj = 0
    i = 0
    while nj < Nj:
        aff_count += [[]]
        for k, juror in enumerate(jurors):
            if len(juror.affiliations) == i:
                aff_count[i] += [k]
                nj += 1
        i += 1

    order = []

    # usually those with more affiliations are few,
    # therefore we can add them at the start without a specific order in mind
    for l_count in reversed(aff_count[2:]):
        for k in l_count:
            order += [k]

    # add the bulk by country, so they can be allocated easily, we do first the country with most jurors
    aff_t_count = [len(aff_t) for aff_t in aff_team_list]
    for t in reversed(np.argsort(aff_t_count)):
        for k in aff_team_list[t]:
            order += [k]

    for k in aff_count[0]:
        order += [k]

    jurors = [jurors[k] for k in order]


###################################
#        Sudoku Algorithm        #
###################################


# same principle as the previous one, but it's all PFs simultaneously
def assign_jurors_sudoku_global():
    global schedule
    global jurors
    global assigned
    global count
    global reached_limit
    count += 1
    if count % 100 == 0:
        sys.stdout.write('\r')
        show_status_global()
    if count > count_limit:
        reached_limit = True
        return

    for k, juror in enumerate(jurors):
        for i in range(nPFs):
            if juror.schedule[i] == -1:
                # ordering rooms according to violations of rule 3
                r_order = np.argsort([evaluate_conflicts_3(jurors[k], pf) for pf in schedule[i]])
                # r_order = [r for r in range(len(schedule[i]))]
                for j in r_order:
                    if len(schedule[i][j].juror_list) < schedule[i].max_j_p_room:
                        if evaluate_new_juror(i, j, k):
                            schedule[i][j].juror_list.append(juror)
                            juror.schedule[i] = j
                            for t in schedule[i][j].team_list:
                                juror.teams_seen[t] += 1
                            schedule[i].Nj += 1

                            assign_jurors_sudoku_global()

                            if assigned:
                                return

                            schedule[i][j].juror_list.pop()
                            juror.schedule[i] = -1
                            for t in schedule[i][j].team_list:
                                juror.teams_seen[t] -= 1
                            schedule[i].Nj -= 1
                # if we cycled through all the rooms and the juror cannot be assigned then this is not a solution
                return
    score = evaluate_conflicts(schedule, jurors)
    if score[2] < best_score_3:
        for pf in schedule:
            for r in pf:
                # we exclude solutions where there is a big imbalance in number of jurors per room
                if len(r.juror_list) < pf.max_j_p_room - 1:
                    return
        assigned = True


#################################
#        Run Algorithm          #
#################################


# import stuff
import_all('teams.txt', 'teams_allocation.txt', 'jurors.txt')

best_score_3 = 999
best_s = None
best_j = None
n = 0

# ---------------------------------- algorithm parameters ---------------------------------- #
count_limit = 70000
prob_ign_rule3 = 0.2
# ---------------------------------- algorithm parameters ---------------------------------- #

empty_s = copy(schedule)
empty_j = copy(jurors)

print('*'*100)
print('*'*100)
print(' ')
print('Starting computation of jury allocation that respects rules #1 and #2')
print('Looking for the combination that violates rule #3 the least')
print(' ')
print('\tsettings:\n\t\t - iteration limit for restart: {},'
      '\n\t\t - probability of ignoring rule #3: {}'.format(count_limit, prob_ign_rule3))

print('\n\t\t ---- press ctrl+c when the score is satisfactory (eg. 50) ---- ')
while True:
    try:
        schedule = copy(empty_s)
        jurors = copy(empty_j)

        n += 1
        print('\nstarting attempt #{}'.format(n))
        # putting the jurors in order by affiliation
        jurors_ordering()

        # initializing global start and stop variables
        assigned = False
        reached_limit = False
        count = 0

        # assign jurors optimizing all PFs simultaneously
        assign_jurors_sudoku_global()

        print(' ')
        if reached_limit:
            print('\t reached iterations manual limit')
            print('\t ------> current best score for rule #3: {}'.format(best_score_3))
            print('\t restarting computation')
            continue

        score = evaluate_conflicts(schedule, jurors)

        if score[2] < best_score_3:
            best_score_3 = score[2]
            best_s = copy(schedule)
            print('\t reached new optimum!')
        print('\t ------> current best score for rule #3: {}'.format(best_score_3))
    except KeyboardInterrupt:
        # stop breeding if prompted
        break

schedule = best_s
jurors = extract_real_jurors(schedule)

score = evaluate_conflicts(schedule, jurors)
print('score : {}'.format(score))

# performing permutations to remove some conflicts of rule #3
print(' ')
print('*'*100)
print(' ')
print('Initiating permutation procedure to get a better score for rule #3')
print('\n')

make_score3_betterv2()

print(' ')
print('Finished!')
print('Final schedule:')



# # print what we have
print_teams(teams)
print_jurors(jurors, teams, schedule)
print_schedule(schedule, teams)

j_matrix = computeJ_matrix(schedule, jurors)
print_matrix(j_matrix)

score = evaluate_conflicts(schedule, jurors)
print('score : {}'.format(score))

###########################
#     Save the result     #
###########################

inp = input('Want to save this schedule? (YES/NO)')

if inp == 'YES':
    print('printing files')
    with open('jurorSchedule.txt', "w") as text_file:
        for juror in jurors:
            output = juror.name + ';' + ';'.join([str(room) for room in juror.schedule])
            print(output, file=text_file)

    with open('judgementMatrix.txt', "w") as text_file:
        for line in j_matrix:
            output = ' '.join([str(item) for item in line])
            print(output, file=text_file)
        print(' ', file=text_file)
        print('(score: {})'.format(score), file=text_file)
